# RSPEC Tutorial

Rspec tutorial from [Udemy](https://www.udemy.com/) video series called [Testing Ruby with RSpec: The Complete Guide](https://www.udemy.com/course/testing-ruby-with-rspec/).

Course taught by [Boris Paskhaver](https://borispaskhaver.com)